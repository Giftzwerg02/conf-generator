module SpecHelper (
    module Test.Hspec,
    readExamples
) where

import Test.Hspec
import Control.Monad
import Data.Semigroup
import qualified Data.Text as T 
import qualified Data.Text.IO as TIO 

{---- 
Function copied from https://haskell-at-work.com/episodes/2018-01-13-dynamic-test-suites-in-haskell-using-hspec-and-tasty.html 
----}
readExamples :: FilePath -> IO [(String, String)]
readExamples filepath = mapM asPair . T.lines =<< TIO.readFile filepath
  where
    asPair line =
      case T.splitOn (T.pack ";") line of
        [input, expected] -> pure (T.unpack input, T.unpack expected)
        _ -> fail ("Invalid example line: " <> T.unpack line)