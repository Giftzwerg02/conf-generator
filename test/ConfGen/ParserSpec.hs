{-# Language OverloadedStrings #-}

module ConfGen.ParserSpec where

import SpecHelper
import ConfGen.Parser
import Test.QuickCheck
import Test.Hspec
import Text.ParserCombinators.Parsec
import Data.IP
import Text.Parsec (Stream)
import Control.Monad
import Text.Printf


ipParserSpec :: Spec
ipParserSpec = do
    describe "The IP-Parser is able to return \"Just <IPRange>\" if" $ do
        it "the given string represents a valid IPRange;" $ do
            let expected = Just (makeAddrRange (toIPv4 [192,168,10,0]) 24 :: AddrRange IPv4)          
                actual = parse ipParser "test" "192.168.10.0/24"
            actual <=> (\res -> (ipv4range <$> res) `shouldBe` expected)

        it "otherwise, it will return Nothing." $ do
            let expected = Nothing :: Maybe IPRange
                actual = parse ipParser "test" "some invalid ip"
            actual <=> (`shouldBe` expected)

interfacePortParserSpec :: Spec
interfacePortParserSpec = do
    describe "The Interface-Port-Parser parses a string of ints seperated by a '/'." $ do
        examples <- runIO $ readExamples "test/ConfGen/interface-port-parser-spec.csv"
        forM_ examples $ \(inputStr, expectedStr) ->
            it (printf "It returns %s when the given string is \"%s\"." expectedStr inputStr) $ do
                let expected =  read expectedStr :: InterfacePort
                    actual = parse interfacePortParser "test" inputStr
                actual <=> (`shouldBe` expected)


interfaceNameParserSpec :: Spec
interfaceNameParserSpec = do
    describe "The Interface-Name-Parser parses a string that starts with either 'f', 'g', 's' or 'v'. That string can only contain letters." $ do
        examples <- runIO $ readExamples "test/ConfGen/interface-name-parser-spec.csv"
        forM_ examples $ \(inputStr, expectedStr) ->
            it (printf "It returns %s when the given string is \"%s\"." expectedStr inputStr) $ do
                    let expected = expectedStr
                        actual = parse interfaceNameParser "test" inputStr
                    actual <=> (`shouldBe` expected)


spec :: Spec
spec = do
    ipParserSpec
    interfacePortParserSpec
    interfaceNameParserSpec

bindParse :: Either a b -> (b -> Expectation) -> Expectation
bindParse res f =
    case res of
        Left a -> failTest
        Right b -> f b

(<=>) :: Either a b -> (b -> Expectation) -> Expectation
(<=>) = bindParse

failTest :: Expectation
failTest = 1 `shouldBe` 2
