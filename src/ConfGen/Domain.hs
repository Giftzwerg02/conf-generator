module ConfGen.Domain where

import Data.IP

data Entry
    = Devices DeviceHead (Maybe Configuration)
    | Network NetworkName (Maybe IPRange)
    deriving (Show)

type DeviceHead = [(Name, Maybe NetworkBindings)]
type DeviceName = String
type NetworkName = String
type Name = String

type NetworkBindings = [NetworkBinding]
data NetworkBinding = NetworkBinding Interface NetworkName deriving(Show)

data Interface = Interface InterfaceName InterfacePort deriving(Show)
type InterfaceName = String
type InterfacePort = [Int]

type Configuration = [ConfigLine]
type ConfigLine = String
