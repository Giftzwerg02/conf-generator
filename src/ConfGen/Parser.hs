module ConfGen.Parser where

import Data.IP
import Text.Parsec.Prim (ParsecT)
import Data.Functor.Identity (Identity)
import Text.Read
import Text.ParserCombinators.Parsec
import Control.Monad (void)
import Text.Parsec (Stream)
import ConfGen.Domain

parseConfGen :: String -> Either ParseError [Entry]
parseConfGen = parse confGenParser "Conf-Gen parser!"

confGenParser :: Parser [Entry]
confGenParser = many entryParser

entryParser :: Parser Entry
entryParser =  betweenSpaces (try networkParser <|> devicesParser)

devicesParser :: Parser Entry
devicesParser = 
    Devices 
    <$> betweenSpaces entryHeadParser
    <*> optionMaybe (char '{' *> betweenSpaces configParser <* char '}' <* spaces)

networkParser :: Parser Entry
networkParser =
    Network
    <$> betweenSpaces nameParser
    <*> (char '(' *> betweenSpaces ipParser <* char ')')

configParser :: Parser Configuration
configParser = lines <$> config
    where config = nonCBrackets

ipParser :: Parser (Maybe IPRange)
ipParser = readMaybe . removeAllWS <$> ipString 
    where ipString = nonBrackets

networkBindingsParser :: Parser NetworkBindings
networkBindingsParser = 
    (many (char ' ') *> networkBindingParser <* many (char ' ')) `sepEndBy` oneOf ";\t\n"

networkBindingParser :: Parser NetworkBinding
networkBindingParser = 
    NetworkBinding <$> interfaceParser <*> ((string "in" *> spaces) *> nameParser)
    
nameParser :: Parser Name
nameParser = many1 (letter <|> digit)

entryHeadParser :: ParsecT String () Identity [(Name, Maybe NetworkBindings)]
entryHeadParser = many1 $ betweenSpaces entry
    where entry = (,) <$> (nameParser <* spaces) <*> optionMaybe (char '[' *> betweenSpaces networkBindingsParser <* char ']')
    
interfaceParser :: Parser Interface
interfaceParser = Interface <$> interfaceNameParser <*> interfacePortParser

interfaceNameParser :: Parser InterfaceName
interfaceNameParser = (:) <$> oneOf interfaceIndicators <*> many letter
    where interfaceIndicators = "fgsv"

interfacePortParser :: Parser InterfacePort
interfacePortParser = map readInt <$> stringDigits 
    where stringDigits =  betweenSpaces (many1 digit) `sepBy` char '/'


{----- Helpers -----}

betweenSpaces :: ParsecT String u Identity a -> ParsecT String u Identity a
betweenSpaces = between spaces spaces

betweenCBrackets :: ParsecT String u Identity a -> ParsecT String u Identity a
betweenCBrackets = between (char '{') (char '}')

betweenSBrackets :: ParsecT String u Identity a -> ParsecT String u Identity a
betweenSBrackets = between (char '[') (char ']')

beforeCAndSBrackets :: ParsecT String u Identity Char
beforeCAndSBrackets = beforeBrackets "{["

beforeBrackets :: String -> ParsecT String u Identity Char
beforeBrackets brackets =
    Text.ParserCombinators.Parsec.choice $ map char brackets

pChoice :: Stream s m t => [ParsecT s u m a] -> ParsecT s u m a
pChoice = Text.ParserCombinators.Parsec.choice

nonCBrackets :: ParsecT String u Identity [Char]
nonCBrackets = many $ noneOf "{}"

nonBrackets :: ParsecT String u Identity [Char]
nonBrackets = many $ noneOf "{}[]()"

removeAllWS :: [Char] -> [Char]
removeAllWS = filter (/= ' ')

readInt :: String -> Int
readInt = read :: String -> Int 